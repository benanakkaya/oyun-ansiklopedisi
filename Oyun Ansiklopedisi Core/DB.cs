﻿using Oyun_Ansiklopedisi_Core.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oyun_Ansiklopedisi_Core
{
    public class DB : DbContext 
        
    {
        //Tablolar
        
        public DB()
            : base(@"Data Source=.\;Initial Catalog=OyunAnsiklopedisiDB;Integrated Security=True")
        {       
        }  

        public DbSet<User> Users { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Platform> Platform { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }

    }
}
