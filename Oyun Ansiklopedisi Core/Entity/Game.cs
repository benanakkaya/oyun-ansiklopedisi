﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oyun_Ansiklopedisi_Core.Entity
{
    public class Game : EntityBase
    {
        public string Name { get; set; }
        public string Producer { get; set; }
        public string RDate { get; set; }
        public string Description { get; set; }
        public string SystemReq { get; set; }
        public decimal UserPoint { get; set; }
        public string ImageUrl { get; set; }
        public int PlatformID { get; set; }
        public Platform Platform { get; set; }
        public int CategoryID { get; set; }
        public Category Category { get; set; }
        public virtual IEnumerable<Comment> Comment { get; set; }

    }
}
