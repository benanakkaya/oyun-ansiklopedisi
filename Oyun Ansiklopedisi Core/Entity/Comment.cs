﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oyun_Ansiklopedisi_Core.Entity
{
    public class Comment : EntityBase
    {
        public int Point { get; set; }
        public int UserID { get; set; }
        public User User { get; set; }
        public int GameID { get; set; }
        public Game Game { get; set; }
    }
}
