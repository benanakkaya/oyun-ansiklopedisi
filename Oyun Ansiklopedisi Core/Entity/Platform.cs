﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oyun_Ansiklopedisi_Core.Entity
{
    public class Platform : EntityBase
    {
        public string Name { get; set; }
        public virtual IEnumerable<Game> Game { get; set; }
    }
}
